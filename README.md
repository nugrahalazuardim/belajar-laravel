Nama	: Muhammad Lajuwardi Nugraha <br>
Alamat	: Perum Sawo Griya Kencana Blok L No 2 Limo Depok Jawa Barat <br>
Email	: nugrahalazuardim@gmail.com <br>

Status : 

## v1.0 
 - [Initial Push]

## v2.0
- Template changed to AdminLTE <br>
- Sub layout added <br>
        -> adminlte (for templating) <br>
        -> example (excercise - week 3) <br>
        -> table (data layout) <br>
- Route updated <br>
        -> "/"            -> admin index <br>
        -> "/table"       -> admin table <br>
        -> "/form"        -> admin form <br>
        -> "/data-tables" -> admin data table <br>
- Asset added with AdminLTE Template <br>
        -> plugin <br>
        -> dist <br>

## v2.1
- Migration created <br>
        -> profiles table <br>
        -> films table <br>
        -> casts table <br>
        -> genres table <br>
        -> peran table <br>
        -> kritik table <br>
- Minor updated <br>
        -> navbar.blade.php edited <br>
        -> sidebar.blade.php edited <br>
        -> home.blade.php edited <br>

## v2.2
- Feature Added <br>
        -> Input new cast <br>
        -> Show cast list <br>
                -> create.blade.php <br>
                -> edit.blade.php <br>
                -> index.blade.php <br>
                -> show.blade.php <br>
- Route updated <br>
        -> "get(/cast)"           -> input new cast  <br>
        -> "post(/cast)"          -> insert new cast to database <br>
        -> "get(/cast)"           -> index for casts table <br>
        -> "get(/cast/{id})"      -> showing data availability <br>
        -> "get(/cast/{id}/edit)" -> edit data from table <br>
        -> "put(/cast/{id})"      -> update data <br>
        -> "delete(/cast)"        -> delete data <br>
- Controller updated <br>
        -> CastController for <b>casts</b> table controlling <br>
- Some minor updated <br>
        -> footer.blade.php edited <br>