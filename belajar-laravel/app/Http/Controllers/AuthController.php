<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('register');
    }

    public function profile(Request $req) {
        // dd($req->all());
        $depan = $req["nama_depan"];
        $belakang = $req["nama_belakang"];
        return view('welcome', compact('depan','belakang'));
    }
}
