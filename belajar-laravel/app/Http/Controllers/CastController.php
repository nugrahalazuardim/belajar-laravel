<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create() 
    {
        return view('casts.create');
    }

    public function store(Request $req) 
    {
        // dd($req->all());
        $req->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required|max:2',
            'bio' => 'required|max:255'
        ]);

        $query = DB::table('casts')->insert([
            "nama" => $req["nama"],
            "umur" => $req["umur"],
            "bio" => $req["bio"]
        ]);

        return redirect('/cast')->with('success', 'Data berhasil di input');
    }

    public function index() 
    {
        $posts = DB::table('casts')->get();
        // dd($post);
        return view('casts.index', compact('posts'));
    }

    public function show($id)
    {
        $post = DB::table('casts')->where('id', $id)->first();
        // dd($post);
        return view('casts.show', compact('post'));
    }

    public function edit($id)
    {
        $post = DB::table('casts')->where('id', $id)->first();
        return view('casts.edit', compact('post'));
    }

    public function update($id, Request $req)
    {
        $req->validate([
            'nama' => 'required',
            'umur' => 'required|max:2',
            'bio' => 'required|max:255'            
        ]);

        $query = DB::table('casts')
                ->where('id', $id)
                ->update([
                    'nama' => $req['nama'],
                    'umur' => $req['umur'],
                    'bio' => $req['bio']
                ]);

                return redirect('/cast')->with('success', 'Berhasil update data');
    }

    public function destroy($id)
    {
        $query = DB::table('casts')
                ->where('id', $id)
                ->delete();
                return redirect('/cast')->with('success', 'Berhasil dihapus');
    }
}
