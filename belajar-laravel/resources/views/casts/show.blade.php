@extends('adminlte.master')

@section('title')
Halaman Detail    
@endsection

@section('sub-title')
Show Detail Data Cast    
@endsection

@section('content')
    <div>
        <h4> {{$post->nama}} </h4>
        <p> {{$post->umur}} </p>
        <p>
            {{$post->bio}}
        </p>
    </div>
@endsection