@extends('adminlte.master')

@section('title')
    Halaman Create
@endsection

@section('sub-title')
    Create
@endsection

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Buat Data Cast Baru</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/cast" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" value="{{old('name', '')}}" placeholder="Enter cast actor name">
          @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="number" class="form-control" id="umur" name="umur" value="{{old('age', '')}}" placeholder="Enter cast actor age">
          @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
          @enderror
        </div>
          <div class="form-group">
            <label>Biography</label>
            <textarea class="form-control" rows="3" placeholder="Write the actor biography here" name="bio">{{old('bio', '')}}</textarea>
            @error('bio')
              <div class="alert alert-danger">{{$message}}</div>
            @enderror
          </div>
      <button type="submit" class="btn btn-primary">Submit</button>
      <!-- /.card-body -->
    </form>
  </div>
@endsection