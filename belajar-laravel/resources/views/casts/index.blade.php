@extends('adminlte.master')

@section('title')
Halaman Show    
@endsection

@section('sub-title')
Show Data    
@endsection

@section('content')
    {{--  <div class="mt-3 ml-3">  --}}
        @if (session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        <a href="/cast/create" class="btn btn-primary mb-2">Create New Cast</a>  
        <div class="card">
            <div class="card-body table-responsive p-0">
              <table class="table table-hover text-nowrap">
                <thead>
                  <tr>
                    <th style="width: 10px">ID</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 200px">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($posts as $item => $post)
                      <tr>
                          <td>{{$item + 1}}</td>
                          <td>{{$post->nama}}</td>
                          <td>{{$post->umur}}</td>
                          <td>{{$post->bio}}</td>
                          <td style="display: flex">
                              <a href="/cast/{{$post->id}}" class="btn btn-info btn-sm">show</a>
                              <a href="/cast/{{$post->id}}/edit" class="btn btn-warning btn-sm ml-2">edit</a>
                              <form action="/cast/{{$post->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm ml-2">
                              </form>
                          </td>
                      </tr>
                    @empty
                    <tr>
                        <td colspan="5" align="center">No Data</td>
                    </tr>

                  @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
    {{--  </div>  --}}
@endsection