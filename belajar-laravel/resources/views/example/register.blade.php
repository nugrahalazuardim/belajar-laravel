<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>Register</title>
    <style>
        label#title, legend {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <div class="col-md-6">
            <form action="/welcome" method="POST">
                @csrf
                <div class="form-group row">
                    <label for="firstNameInput" class="col-sm-2 col-form-label" id="title">First Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_depan" id="nama_depan" placeholder="Your First Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="lastNameInput" class="col-sm-2 col-form-label" id="title">Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_belakang" id="nama_belakang" placeholder="Your Last Name">
                    </div>
                </div>
                <fieldset class="form-group row">
                    <legend class="col-form-label col-sm-2 float-sm-left pt-0">Gender</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios" value="Male">
                            <label class="form-check-label" for="exampleRadios1">Male</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="pr" value="Female">
                            <label class="form-check-label" for="exampleRadios1">Female</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="other" value="Other">
                            <label class="form-check-label" for="exampleRadios1">Other</label>
                        </div>
                </fieldset>
                <div class="form-group row">
                    <label for="nationalityOption" class="col-sm-2 col-form-label" id="title">Nationality</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="kebangsaan" id="kebangsaan">
                            <option>Indonesia</option>
                            <option>Singapore</option>
                            <option>Malaysia</option>
                            <option>Thailand</option>
                        </select>
                    </div>
                </div>
                <fieldset class="form-group row">
                    <legend class="col-form-label col-sm-2 float-sm-left pt-0">Language Spoken</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="exampleCheckbox" id="id" value="Bahasa Indonesia">
                            <label class="form-check-label" for="exampleRadios1">Bahasa Indonesia</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="en" value="English">
                            <label class="form-check-label" for="exampleRadios1">English</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="ar" value="Arabic">
                            <label class="form-check-label" for="exampleRadios1">Arabic</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="jp" value="Japanese">
                            <label class="form-check-label" for="exampleRadios1">Japanese</label>
                        </div>
                </fieldset>
                <div class="form-group">
                    <label for="bioInput" id="title">Bio</label>
                    <textarea class="form-control" name="bio" id="bio" rows="5"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

</body>
</html>