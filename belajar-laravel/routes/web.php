<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@show_home');
Route::get('/register', 'AuthController@form');
Route::post('/welcome', 'AuthController@profile');
Route::get('/admin', 'AdminController@admin_home');

// Route::get('/casts', 'CastController@index');

Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{id}', 'CastController@show');
Route::get('/cast/{id}/edit', 'CastController@edit');
Route::put('/cast/{id}', 'CastController@update');
Route::delete('/cast/{id}', 'CastController@destroy');

Route::get('/table', function() {
    return view('table.table');
});

Route::get('/data-tables', function() {
    return view('table.datatable');
});